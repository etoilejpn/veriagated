#!/bin/sh

python3 \
    ./tools/print_instance.py \
    ./verible/bazel-bin/verilog/tools/syntax/verible-verilog-syntax \
    ./src/apb2spi/trunk/rtl/APB_SLAVE.v \
    ./src/apb2spi/trunk/rtl/APB_SPI_Top.v