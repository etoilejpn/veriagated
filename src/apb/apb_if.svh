`ifndef __APB_IF_SVH__
`define __APB_IF_SVH__

interface apb_if (
	input PCLK,
	input PRESETn
);
	localparam DATA_WIDTH = 8;
	localparam ADDR_WIDTH = 3;

	modport master (
		input						PCLK,
		input						PRESETn,
		output	[ADDR_WIDTH-1:0]	PADDR,
		output						PSEL,
		output						PENABLE,
		output						PWRITE,
		output	[DATA_WIDTH-1:0]	PWDATA,
		input						PREADY,
		input	[DATA_WIDTH-1:0]	PRDATA,
		input						PSLVERR
	);

	modport slave (
		input						PCLK,
		input						PRESETn,
		input	[ADDR_WIDTH-1:0]	PADDR,
		input						PSEL,
		input						PENABLE,
		input						PWRITE,
		input	[DATA_WIDTH-1:0]	PWDATA,
		output						PREADY,
		output	[DATA_WIDTH-1:0]	PRDATA,
		output						PSLVERR
	);
endinterface

`endif