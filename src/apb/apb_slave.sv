`include "apb_if.svh"

module apb_slave (
	apb_if.slave apb_if_slave
);
	typedef enum {
		IDLE,
		SETUP,
		ACCESS
	} state_type;

	state_type state_t, next_state_t;

	always_comb
		case (state_t)
			IDLE:
				if (apb_if_slave.PSEL & ~apb_if_slave.PENABLE)
					next_state_t = SETUP;
				else
					next_state_t = IDLE;
			SETUP:
				if (apb_if_slave.PENABLE)
					next_state_t = ACCESS;
				else
					next_state_t = IDLE;
			ACCESS:
				if (apb_if_slave.PENABLE)
					next_state_t = SETUP;
				else
					next_state_t = IDLE;
		endcase

	always_ff @(posedge apb_if_slave.PCLK or negedge apb_if_slave.PRESETn)
		if (~apb_if_slave.PRESETn)
			state_t <= IDLE;
		else
			state_t <= next_state_t;

	always_ff @(posedge apb_if_slave.PCLK or negedge apb_if_slave.PRESETn)
		if (~apb_if_slave.PRESETn)
			apb_if_slave.PREADY <= 0;
		else
			if (next_state_t == IDLE)
				apb_if_slave.PREADY <= 1;
			else
				apb_if_slave.PREADY <= 0;

	assign write_en =  apb_if_slave.PWRITE & (state_t == ACCESS);
	assign read_en  = ~apb_if_slave.PWRITE & (state_t == SETUP);


endmodule
