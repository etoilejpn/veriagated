import platform

class OS:
    """Output of running OS name.

    This class imports platform class.

    Returns:
        string    OS
          'nt'      Windows
          'darwin'  Mac
          'posix'   Linux
    """
    @classmethod
    def name(self):
        pf = platform.system()
        if pf == 'Windows':
            os_name = 'nt'
            #print('on Windows')
        elif pf == 'Darwin':
            os_name = 'darwin'
            #print('on Mac')
        elif pt == 'Linux':
            os_name = 'posix'
            #print('on Linux')
        return os_name
