import sys
sys.path.append('anytree')
sys.path.append('six')
from typing import Optional
from anytree import Node, RenderTree

class Instance(Node):
    def __init__(self, name: str, module: Optional[str] = None, parent: Optional["Instance"] = None):
        super().__init__(parent)
        self.name = name
        self.module = module
        self.parent = parent

instanece_root = Instance("inst_root")
instanece_s0 = Instance("inst_s0", parent=instanece_root)
instanece_s1 = Instance("inst_s1", parent=instanece_root)
instanece_s2 = Instance("inst_s2", parent=instanece_s0)
foo_num = '4'

root = Node("root")
s0 = Node("sub0", parent=root)
s0b = Node("sub0B", parent=s0, foo=foo_num, bar=109)
s0a = Node("sub0A", parent=s0)
s1 = Node("sub1", parent=root)
s1a = Node("sub1A", parent=s1)
s1b = Node("sub1B", parent=s1, bar=8)
s1c = Node("sub1C", parent=s1)
s1ca = Node("sub1Ca", parent=s1c)

print(RenderTree(root))
print(RenderTree(instanece_root))